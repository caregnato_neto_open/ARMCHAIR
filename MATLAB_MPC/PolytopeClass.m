classdef PolytopeClass < handle
    
    properties
       op_region = [];
       obs = [];
       tar = [];
       conn_reg = [];
       nominal_conn_reg = [];
       robust_conn_reg = [];
       agent_body = [];
       vel = [];
       acc = [];
       agt = [];
       scen = [];
       human_safety_region = [];
       teste = [];
    end
    
    methods
        function obj = PolytopeClass(agt,scen)
            
            % 
            h_agt_body_x = agt(1).specs.width/2;
            h_agt_body_y = agt(1).specs.width/2;
            
            % Operation region
            obj.op_region.P = [eye(2);-eye(2)];
            obj.op_region.q = [
                scen.op_region_center(1) + scen.op_region_size_x/2 - h_agt_body_x;
                scen.op_region_center(2) + scen.op_region_size_y/2 - h_agt_body_y;
                scen.op_region_center(1) + scen.op_region_size_x/2 - h_agt_body_x;
                scen.op_region_center(2) + scen.op_region_size_y/2 - h_agt_body_y
                ];
            
            % Obstacles
            for i = 1 : scen.number_obs
                obj.obs{i}.P = [eye(2);-eye(2)];
                obj.obs{i}.q = [scen.obs.center{i}(1) + scen.obs.dimensions{i}(1)/2 + h_agt_body_x;
                    scen.obs.center{i}(2) + scen.obs.dimensions{i}(2)/2 + h_agt_body_y;
                    -(scen.obs.center{i}(1) - scen.obs.dimensions{i}(1)/2) + h_agt_body_x;
                    -(scen.obs.center{i}(2) - scen.obs.dimensions{i}(2)/2) + h_agt_body_y];
            end
            
            % Targets
            for i = 1 : scen.number_tar
                obj.tar{i}.P = [eye(2);-eye(2)];
                obj.tar{i}.q = [scen.tar.center{i}(1) + scen.tar.dimensions{i}(1)/2;
                    scen.tar.center{i}(2) + scen.tar.dimensions{i}(2)/2;
                    -(scen.tar.center{i}(1) - scen.tar.dimensions{i}(1)/2);
                    -(scen.tar.center{i}(2) - scen.tar.dimensions{i}(2)/2)];
            end
            
            % Robot bodies
               
%             % ONLY BODIES
%             for i = 1 : scen.number_agents
%                 % Body polytope
%                 obj.agent_body{i}.P = [eye(2);-eye(2)];
%                 obj.agent_body{i}.q = [agt(i).specs.width/2;
%                     agt(i).specs.length/2;
%                     agt(i).specs.width/2;
%                     agt(i).specs.length/2];
%             end

               % Considering bodies for robots 
               for i = 1 : scen.number_agents
                   % Body polytope
                   obj.agent_body{i}.P = [eye(2);-eye(2)];
                   obj.agent_body{i}.q = [agt(i).specs.width/2;
                       agt(i).specs.length/2;
                       agt(i).specs.width/2;
                       agt(i).specs.length/2];
               end
               
               %and safety region for human
               % centered at human position
%                                vertices_sr(1,:) = [scen.cell_size_x + agt(end).specs.width/2 , scen.cell_size_y + agt(end).specs.length/2]; %(+,+)
%                                vertices_sr(2,:) = [-(scen.cell_size_x + agt(end).specs.width/2) , scen.cell_size_y + agt(end).specs.length/2]; % (-,+)
%                                vertices_sr(3,:) = [-(scen.cell_size_x + agt(end).specs.width/2) , -(scen.cell_size_y + agt(end).specs.length/2)]; % (-,-)
%                                vertices_sr(4,:) = [scen.cell_size_x + agt(end).specs.width/2 , -(scen.cell_size_y + agt(end).specs.length/2)];%(+,-)
               
               vertices_sr(1,:) = [1.5*scen.cell_size_x , 1.5*scen.cell_size_y ]; %(+,+)
               vertices_sr(2,:) = [-(1.5*scen.cell_size_x) , 1.5*scen.cell_size_y ]; % (-,+)
               vertices_sr(3,:) = [-(1.5*scen.cell_size_x ) , -(1.5*scen.cell_size_y )]; % (-,-)
               vertices_sr(4,:) = [1.5*scen.cell_size_x  , -(1.5*scen.cell_size_y )];%(+,-)
%                
               
               human_safety_region = Polyhedron(vertices_sr);
               human_safety_region.computeHRep();
               obj.human_safety_region.P = human_safety_region.A;
               obj.human_safety_region.q = human_safety_region.b;
               



            
            for i = 1 : scen.number_agents
                % Velocities
                % Build octagon region to bound global v_x and v_y given a maximum linear velocity
                max_lin_vel = agt(i).specs.max_lin_vel; % Turtlebot3 max velocity
                vertices(1,:) = [0, max_lin_vel];
                vertices(2,:) = [max_lin_vel*sqrt(2)/2, max_lin_vel*sqrt(2)/2];
                vertices(3,:) = [max_lin_vel,0];
                vertices(4,:) = [max_lin_vel*sqrt(2)/2, -max_lin_vel*sqrt(2)/2];
                vertices(5,:) = [0, -max_lin_vel];
                vertices(6,:) = [-max_lin_vel*sqrt(2)/2, -max_lin_vel*sqrt(2)/2];
                vertices(7,:) = [-max_lin_vel, 0];
                vertices(8,:) = [-max_lin_vel*sqrt(2)/2, max_lin_vel*sqrt(2)/2];
                
                vel_bound_set = Polyhedron(vertices);
                vel_bound_set.computeHRep;
                obj.vel{i}.P = vel_bound_set.A;
                obj.vel{i}.q = vel_bound_set.b;
                
                %acceleration
                obj.acc{i}.P = [eye(2);-eye(2)];
                obj.acc{i}.q = [agt(i).specs.max_acc; agt(i).specs.max_acc; -agt(i).specs.min_acc; -agt(i).specs.max_acc];
            end
            
            % Connectivity regions
            % There are two regions:
            % a) a nominal one for connections between robots (radius 2.5 m)
            % b) robust one for connections with human (radius 1.5 m)
            %
            % This is just for planning, of course. In practice they are
            % all 2.5m
            %
            % obs: it is a difference of 1.0 m because the maximum error in
            % prediction of the human results in a 1.0 meter difference (it
            % is the size of the cell
            
            % Nominal
            conn_region_vertices = [
                agt(1).specs.conn_region_radius , 0;
                agt(1).specs.conn_region_radius*cos(pi/4) , agt(1).specs.conn_region_radius*cos(pi/4);
                0 , agt(1).specs.conn_region_radius;
                -agt(1).specs.conn_region_radius*cos(pi/4) , agt(1).specs.conn_region_radius*cos(pi/4);
                -agt(1).specs.conn_region_radius , 0;
                -agt(1).specs.conn_region_radius*cos(pi/4) , -agt(1).specs.conn_region_radius*cos(pi/4);
                0 , -agt(1).specs.conn_region_radius;
                agt(1).specs.conn_region_radius*cos(pi/4) , -agt(1).specs.conn_region_radius*cos(pi/4)
                ];
            
            aux_poly = Polyhedron(conn_region_vertices);
            aux_poly.computeHRep();
            
            obj.nominal_conn_reg.P = aux_poly.A;
            obj.nominal_conn_reg.q = aux_poly.b;
            
            
            % Robust test
            vert = [1,1;1,-1;-1,-1;-1,1];
            rob_poly = Polyhedron(vert);
            teste_poly = aux_poly - rob_poly;
            teste_poly.computeHRep();
            teste_poly.minHRep();
                        
            obj.robust_conn_reg.P = teste_poly.A;
            obj.robust_conn_reg.q = teste_poly.b;
            
            % Robust
%             shrinked_radius = agt(1).specs.conn_region_radius - scen.cell_size_y; % use y or x because in this case the cells are squared
%             robust_conn_region_vertices = [
%                 shrinked_radius , 0;
%                 shrinked_radius*cos(pi/4) , shrinked_radius*cos(pi/4);
%                 0 , shrinked_radius;
%                 -shrinked_radius*cos(pi/4) , shrinked_radius*cos(pi/4);
%                 -shrinked_radius , 0;
%                 -shrinked_radius*cos(pi/4) , -shrinked_radius*cos(pi/4);
%                 0 , -shrinked_radius;
%                 shrinked_radius*cos(pi/4) , -shrinked_radius*cos(pi/4)
%                 ];
%             
%             aux_poly = Polyhedron(robust_conn_region_vertices);
%             aux_poly.computeHRep();
%             
%             obj.robust_conn_reg.P = aux_poly.A;
%             obj.robust_conn_reg.q = aux_poly.b;
            
            

                
    
            
            % Nominal
%             for i = 1 : scen.number_agents
%                 
%                 conn_region_vertices = [
%                     agt(i).specs.conn_region_radius , 0;
%                     agt(i).specs.conn_region_radius*cos(pi/4) , agt(i).specs.conn_region_radius*cos(pi/4);
%                     0 , agt(i).specs.conn_region_radius;
%                     -agt(i).specs.conn_region_radius*cos(pi/4) , agt(i).specs.conn_region_radius*cos(pi/4);
%                     -agt(i).specs.conn_region_radius , 0;
%                     -agt(i).specs.conn_region_radius*cos(pi/4) , -agt(i).specs.conn_region_radius*cos(pi/4);
%                     0 , -agt(i).specs.conn_region_radius;
%                     agt(i).specs.conn_region_radius*cos(pi/4) , -agt(i).specs.conn_region_radius*cos(pi/4)
%                     ];
%                 
%                 aux_poly = Polyhedron(conn_region_vertices);
%                 aux_poly.computeHRep();
%                 
%                 obj.conn_reg{i}.P = aux_poly.A;
%                 obj.conn_reg{i}.q = aux_poly.b;
%                 
%      
%                 
%                 
%             end
%             
        end % end function
        
 
      
        
    end % end methods
end % end class

