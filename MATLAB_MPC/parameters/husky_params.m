function husky_pars = husky_params(type)


    if lower(type) == "husky_double_integrator"
        husky_pars.length = 0.990;
        husky_pars.width = 0.670;
        husky_pars.max_lin_vel = 1.0;
        husky_pars.min_lin_vel = 0;
        husky_pars.max_acc = 1.0;
        husky_pars.min_acc = -1.0;
        husky_pars.conn_region_radius = 2.5;
        
        % Dynamics approximated to DI
        husky_pars.dyn.A = [0 1 0 0;0 0 0 0;0 0 0 1;0 0 0 0];
        husky_pars.dyn.B = [0 0; 1 0; 0 0; 0 1];
        husky_pars.dyn.C = eye(4);
        husky_pars.dyn.D = zeros(4,2);
        
        % Discretization
        Ts = 1;
        [Ad,Bd,Cd,Dd] = c2dm(husky_pars.dyn.A,husky_pars.dyn.B,husky_pars.dyn.C,husky_pars.dyn.D,Ts);
        husky_pars.dyn.Ad = Ad;
        husky_pars.dyn.Bd = Bd;
        husky_pars.dyn.Cd = Cd;
        husky_pars.dyn.Dd = Dd; 
    else
        husky_pars = [];
        disp("Wrong agent type");
    end


end