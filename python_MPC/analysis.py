
from ppo import *
import gym
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import pickle
import os

class Analysis:

    def __init__(self,ext_agt,mrs,mp):

        self.ext_agt = ext_agt
        self.mrs = mrs
        self.mp = mp # motion planner


    def export_data_for_analysis(self,data,filename):

        #data = [self.mrs.robot, self.ext_agt.cont_traj, self.mp.predicted_trajectories]

        pickle.dump(data,open(filename + '.pckl' ,'wb'))

    def import_data_for_analysis(self,data_file):
        
        data = pickle.load(open(data_file,'rb'))
        return data
        #self.mrs.robot, self.ext_agt.cont_traj, self.mp.predicted_trajectories, wtv_by_agent

    def incorporate_exp_data(self,exp_data):
        # data vector order: [self.mrs.robot, self.ext_agt.cont_traj, self.mp.predicted_trajectories]
        self.mrs.robot = exp_data[0]
        self.ext_agt.cont_traj = exp_data[1]
        self.mp.predicted_trajectories = exp_data[2]
        self.mp.WTV_by_agent = exp_data[3]
        
    def incorporate_matlab_data(self,ml_data):

        n_robots = len(ml_data['WTV_by_agent'])

        for i in range(n_robots):

            # Updates visited targets 
            self.mp.WTV_by_agent[i] = ml_data['WTV_by_agent'][i]

            # Resets mrs.robot dict to rewrite pos
            self.mrs.robot[i]['pos'] = []

            # MATLAB variable name
            id = i+1
            var_name = 'robot' + str(id) + '_traj'

            # Remove zeros (position after mission finishes)
            rx = ml_data[var_name][0][ml_data[var_name][0] != 0.00000]
            ry = ml_data[var_name][2][ml_data[var_name][2] != 0.00000]

            # find the maximum horizon considering human and robot trajectories
            max_hor = max(len(rx),len(self.ext_agt.cont_traj))

            for k in range(max_hor):
                
                # Keep robot in the same position if k > robot horizon
                if  k >= len(rx): # this will break if the trajectory of the human is bigger than robot
                    rx = np.append(rx,rx[-1])
                    ry = np.append(ry,ry[-1])

                # Assign values
                self.mrs.robot[i]['pos'].append([rx[k],ry[k]])
        
    def remove_robots_data(self):
        # Simply removes robot data for evaluation of human-only scenario
        for i in range(len(self.mrs.robot)):
            for k in range(len(self.mrs.robot[i]['pos'])):
                self.mrs.robot[i]['pos'][k] = self.mrs.robot[i]['pos'][0]  # removes robots from field


        print('ATTENTION: this run is human-only; robot data is being ignored!')
        print('Metrics such as collisions/disconnections will be different from zero, although they are zero')

    def plot_snapshots(self,save_snapshots,plot_snapshots,n,isClosedLoop,plotPredictions):

        robot_colors = ['C1','C2','C3']
        traj_len = len(np.stack(self.ext_agt.cont_traj,axis=0))

        for k in range(traj_len):
            ax = self.plot_continuous_env()
            ax.set_yticklabels([])
            ax.set_xticklabels([])
            plt.tick_params(
                    axis='both',       # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom=False,      # ticks along the bottom edge are off
                    top=False,         # ticks along the top edge are off
                    left=False,
                    right=False,
                    labelbottom=False) # labels along the bottom edge are off

            # Trajectory robots
            for i in range(len(self.mrs.robot)):
                coords_robots = np.stack(self.mrs.robot[i]['pos'],axis=0)
                plt.plot(coords_robots[0:k+1,0],coords_robots[0:k+1,1],robot_colors[i])
                if isClosedLoop and plotPredictions:
                    plt.plot(self.mrs.robot[i]['pred_pos'][k][0],self.mrs.robot[i]['pred_pos'][k][1],color=robot_colors[i],linestyle='dashed',marker='o',markersize='4',alpha=0.4)
                elif plotPredictions:
                    plt.plot(self.mrs.robot[i]['pred_pos'][0][0],self.mrs.robot[i]['pred_pos'][0][1],color=robot_colors[i],linestyle='dashed',marker='o',markersize='4',alpha=0.4)
                # plot body
                robot_offset_x = self.mrs.robot_width/2
                robot_offset_y = self.mrs.robot_length/2
                rect = patches.Rectangle((coords_robots[k,0]-robot_offset_x, coords_robots[k,1]-robot_offset_y),  2*robot_offset_x, 2*robot_offset_y, linewidth=0, edgecolor='k', facecolor=robot_colors[i],alpha=0.2)
                ax.add_patch(rect)


            # Trajectory external agent
            coords = np.stack(self.ext_agt.cont_traj,axis=0)
            plt.plot(coords[0:k+1,1],coords[0:k+1,0],robot_colors[-1])
            if isClosedLoop and plotPredictions:
                predicted_traj = np.stack(self.mp.predicted_trajectories[k],axis=0)
                for z in range(len(predicted_traj)):
                    if predicted_traj[z][0] > 10:
                        break

                plt.plot(predicted_traj[0:z,1],predicted_traj[0:z,0],color=robot_colors[-1],linestyle='dashed',marker='o',markersize='4',alpha=0.4)
            elif plotPredictions:
                predicted_traj = np.stack(self.mp.predicted_trajectories[0],axis=0) # just initial
                for z in range(len(predicted_traj)):
                    if predicted_traj[z][0] > 10:
                        break
                plt.plot(predicted_traj[0:z,1],predicted_traj[0:z,0],color=robot_colors[-1],linestyle='dashed',marker='o',markersize='4',alpha=0.4)

            # plot body
            human_offset_x = self.ext_agt.human_width/2
            human_offset_y = self.ext_agt.human_length/2
            rect = patches.Rectangle((coords[k,1]-human_offset_x, coords[k,0]-human_offset_y),  2*human_offset_x, 2*human_offset_y, linewidth=0, edgecolor='k', facecolor=robot_colors[-1],alpha=0.2)
            ax.add_patch(rect)

            if k < 10:
                prefix = '0'
            else:
                prefix = ''

            if save_snapshots:
                path = './snapshots/exp_' + str(n) +'/'
                dir_exists = os.path.exists(path)

                if dir_exists: # if dir exists -> save
                    plt.savefig(path  + prefix + str(k) + '.svg', format='svg', dpi=1200)
                    #plt.savefig(path  + prefix + str(k) + '.png')
                else: # otherwise, create dir and save
                    os.makedirs(path)
                    plt.savefig(path  + prefix + str(k) + '.svg', format='svg', dpi=1200)

            if not plot_snapshots:
                matplotlib.use('Agg') # This solves the problem of memory leaks when plotting a lot of figures with matplolib
                plt.figure().clear()
                plt.close()
                plt.cla()
                plt.clf()   

            
        if plot_snapshots:
            plt.show()

    def plot_only_external_agent_snapshots(self,save_snapshots,plot_snapshots):
        
        robot_colors = ['C1','C2','C3']

        for k in range(len(np.stack(self.ext_agt.cont_traj,axis=0))):
            ax = self.plot_continuous_env()

            # Trajectory external agent
            coords = np.stack(self.ext_agt.cont_traj,axis=0)
            #plt.plot(coords[0:k+1,1],coords[0:k+1,0],robot_colors[-1])
            predicted_traj = np.stack(self.mp.predicted_trajectories[k],axis=0)
            intended_traj = np.stack(self.ext_agt.intended_trajectories[k],axis=0)
            line1 = ax.plot(intended_traj[:,1],intended_traj[:,0],color='blue',marker='o',markersize='4',alpha=1,label='Intended')
            line2 = ax.plot(predicted_traj[:,1],predicted_traj[:,0],color=robot_colors[-1],linestyle='dashed',marker='o',markersize='4',alpha=1,label='Predicted')
            ax.legend(['Intended', 'Predicted'])
            # plot body
            human_offset_x = self.ext_agt.human_width/2
            human_offset_y = self.ext_agt.human_length/2
            rect = patches.Rectangle((coords[k,1]-human_offset_x, coords[k,0]-human_offset_y),  2*human_offset_x, 2*human_offset_y, linewidth=0, edgecolor='k', facecolor=robot_colors[-1],alpha=0.2)
            ax.add_patch(rect)

            if k < 10:
                prefix = '0'
            else:
                prefix = ''

            if save_snapshots:
                plt.savefig('./snapshots_extAgt_only/' + prefix + str(k) + '.png')
        
        if plot_snapshots:
            plt.show()

    def plot_continuous_env(self):

        half_w = self.ext_agt.param_cont_scen["Width"]/2
        half_l = self.ext_agt.param_cont_scen["Length"]/2
        half_c_size_x = self.ext_agt.param_cont_scen["x_cell_size"]/2
        half_c_size_y = self.ext_agt.param_cont_scen["y_cell_size"] /2


        fig, ax = plt.subplots(figsize=(4, 4))
        plt.grid()
        plt.xticks(np.arange(-2.5, 3.5, step=1))
        plt.yticks(np.arange(-2.5, 3.5, step=1))
        #plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, hspace = 0, wspace = 0)
        #plt.axis('off')
        plt.tight_layout()
        #plt.xlim((-half_w-0.5,half_w+0.5))
        #plt.ylim((-half_l-0.5,half_l+0.5))
        plt.xlim((-half_w,half_w))
        plt.ylim((-half_l,half_l))

        # Field
        #rect_line = patches.Rectangle((-half_w, -half_l),  2*half_w, 2*half_l , linewidth=2, edgecolor='red', facecolor='white',alpha=1)
        rect = patches.Rectangle((-half_w, -half_l),  2*half_w, 2*half_l , linewidth=0, edgecolor='white', facecolor='black',alpha=0.00)
        
       # ax.add_patch(rect_line)
        ax.add_patch(rect)

        # Obstacles
        obs_cells = self.ext_agt.ini_states[3].copy()
        obs_coords = np.stack(np.where(obs_cells==1),axis=0) 
        for i in range(len(obs_coords[0])):
            cont_obs_coords = self.ext_agt.discrete_to_continuous_coord(obs_coords[:,i])
            rect = patches.Rectangle((cont_obs_coords[1]-half_c_size_x, cont_obs_coords[0]-half_c_size_y),  2*half_c_size_x, 2*half_c_size_y, linewidth=0, edgecolor='k', facecolor='k')
            ax.add_patch(rect)

        # Target A
        tar_A_cells = self.ext_agt.ini_states[1].copy()
        tar_A_coords =np.stack(np.where(tar_A_cells==1),axis=0)
        for i in range(len(tar_A_coords[0])):
           cont_tarA_coords = self.ext_agt.discrete_to_continuous_coord(tar_A_coords[:,i])
           rect = patches.Rectangle((cont_tarA_coords[1]-half_c_size_x, cont_tarA_coords[0]-half_c_size_y),  2*half_c_size_x, 2*half_c_size_y, linewidth=0, edgecolor='k', facecolor='y',alpha=0.6)
           ax.add_patch(rect)

        # Target B
        tar_B_cells = self.ext_agt.ini_states[2].copy()
        tar_B_coords =np.stack(np.where(tar_B_cells==1),axis=0)
        for i in range(len(tar_B_coords[0])):
           cont_tarB_coords = self.ext_agt.discrete_to_continuous_coord(tar_B_coords[:,i])
           rect = patches.Rectangle((cont_tarB_coords[1]-half_c_size_x, cont_tarB_coords[0]-half_c_size_y),  2*half_c_size_x, 2*half_c_size_y, linewidth=0, edgecolor='k', facecolor='C10',alpha=0.6)
           ax.add_patch(rect)

        # Terminal cell
        terminal_location = self.ext_agt.env.terminal_location
        cont_terminal_coord = self.ext_agt.discrete_to_continuous_coord(terminal_location)
        rect = patches.Rectangle((cont_terminal_coord[1]-half_c_size_x, cont_terminal_coord[0]-half_c_size_y),  2*half_c_size_x, 2*half_c_size_y, linewidth=0, edgecolor='k', facecolor='m',alpha=0.4)
        ax.add_patch(rect)

        return ax
  
    def plot_trajectories(self):

        self.plot_continuous_env()

        # Trajectory external agent
        coords = np.stack(self.ext_agt.cont_traj,axis=0)
        plt.plot(coords[:,1],coords[:,0],'b')
        
        # Trajectory robots
        for i in range(len(self.mrs.robot)):
            coords_robots = np.stack(self.mrs.robot[i]['pos'],axis=0)
            plt.plot(coords_robots[:,0],coords_robots[:,1],'c')

        traj_length = len(self.ext_agt.cont_traj)
        for k in range(traj_length):
            coord = self.ext_agt.cont_traj[k]

            if k > 0:
                plt.plot(coord[1], coord[0],'bo')
                plt.plot(self.mrs.robot[0]['pos'][k][0],self.mrs.robot[0]['pos'][k][1],'co')
            else:
                plt.plot(coord[1], coord[0],'b*')
                plt.plot(self.mrs.robot[0]['pos'][k][0],self.mrs.robot[0]['pos'][k][1],'c*')

        plt.show()

    def plot_discrete_env(ext_agt):

        env = ext_agt.env

        tar_A_cells = ext_agt.ini_states[1].copy()
        tar_B_cells = ext_agt.ini_states[2].copy()
        obs_cells = ext_agt.ini_states[3].copy()
        terminal_location = ext_agt.env.terminal_location
        initial_location = ext_agt.env.ini_agent_location
        
        fig, ax = plt.subplots()
        plt.grid(axis='both', color='k',linewidth=2)
        plt.xticks(np.arange(-0.5, env.size_hor-0.5, step=1))
        plt.yticks(np.arange(-0.5, env.size_ver-0.5, step=1))
        plt.xlim((-0.5,env.size_hor-0.5))
        plt.ylim((-0.5,env.size_ver-0.5))
        plt.gca().invert_yaxis()

        obs_coords = np.stack(np.where(obs_cells==1),axis=0) 
        for i in range(len(obs_coords[0])):
            rect = patches.Rectangle((obs_coords[1][i]-0.5, obs_coords[0][i]-0.5),  1, 1, linewidth=1, edgecolor='k', facecolor='k')
            ax.add_patch(rect)

        # plot targets
        tar_A_coords =np.stack(np.where(tar_A_cells==1),axis=0)
        for i in range(len(tar_A_coords[0])):
           rect = patches.Rectangle((tar_A_coords[1][i]-0.5, tar_A_coords[0][i]-0.5),  1, 1, linewidth=1, edgecolor='k', facecolor='g')
           ax.add_patch(rect)

    
        tar_B_coords =np.stack(np.where(tar_B_cells==1),axis=0)
        for i in range(len(tar_B_coords[0])):
           rect = patches.Rectangle((tar_B_coords[1][i]-0.5, tar_B_coords[0][i]-0.5),  1, 1, linewidth=1, edgecolor='k', facecolor='r')
           ax.add_patch(rect)

        # temrinal cell
        rect = patches.Rectangle((terminal_location[1]-0.5, terminal_location[0]-0.5),  1, 1, linewidth=1, edgecolor='k', facecolor='m')
        ax.add_patch(rect)

        # initial position
        plt.plot(initial_location[1]-0.4, initial_location[0],'ms')

        print('ter_pos:',terminal_location)


        # Trajectory
        traj_length = len(ext_agt.disc_traj)
        for k in range(traj_length):
            coord = ext_agt.disc_traj[k]
            plt.plot(coord[1], coord[0],'bo')


        plt.show()

    def check_collisions(self):

        # Collision polytope 
        #epsilon = 0.01 # small margin
        P = np.matrix([[1.0,0.0],[0.0,1.0],[-1.0,0.0],[0.0,-1.0]]) 
        q = np.array([self.ext_agt.human_width/2 + self.mrs.robot_width/2, 
                           self.ext_agt.human_length/2 + self.mrs.robot_length/2, 
                           self.ext_agt.human_width/2 + self.mrs.robot_width/2, 
                           self.ext_agt.human_length/2 + self.mrs.robot_length/2])

        # External agent positions
        ext_agt_pos = np.stack(self.ext_agt.cont_traj,axis=0)
        ext_agt_pos[:, [0, 1]] = ext_agt_pos[:, [1, 0]] # ext_agt_pos is inverted in x,y - inverts back
        traj_len = len(ext_agt_pos)
        n_robots = len(self.mrs.robot)

        isThereCollision = np.zeros([traj_len,n_robots])
        isThereInterCollision = np.zeros([traj_len,n_robots])
        
        # Checks for collisions at exact time steps k, k+1, ....
        for i in range(n_robots):
            robots_pos = np.stack(self.mrs.robot[i]['pos'],axis=0)

            for k in range(traj_len):
                #isThereCollision[k] = False # resets

                # Verifies collision at time step 'k'
                rel_pos = ext_agt_pos[k,:] - robots_pos[k,:] 
                collision_inequality = -P*np.matrix(rel_pos).T <= -np.matrix(q).T 
                isThereCollision[k,i] = not any(collision_inequality)

                if isThereCollision[k,i]:
                    print('Collision time step: ',k, ' with robot ', i)
                    #print('Pos ext agt: ',ext_agt_pos[k,:], 'Pos robot',robots_pos[k,:] )
        


        # Checks for collisions between time steps
        n_interstep_points = 10
        alpha = np.arange(0.1,1.0,1.0/n_interstep_points)

        for i in range(len(self.mrs.robot)):
            robots_pos = np.stack(self.mrs.robot[i]['pos'],axis=0)

            for k in range(traj_len-1):

                #if there is no collision, verigy interstep collisions
                if not isThereCollision[k,i] and not isThereCollision[k+1,i] and  k <= traj_len-1: 
                   
                    #next_rel_pos = ext_agt_pos[k+1,:] - robots_pos[k+1,:]

                    for z in range(n_interstep_points-1): # extremes are not necessary
                        inter_ext_agt_pos = alpha[z]*ext_agt_pos[k,:] + (1-alpha[z])*ext_agt_pos[k+1,:]
                        inter_robot_pos = alpha[z]*robots_pos[k,:] + (1-alpha[z])*robots_pos[k+1,:]
                        inter_rel_pos = inter_ext_agt_pos - inter_robot_pos
                        inter_collision_inequality = -P*np.matrix(inter_rel_pos).T <= -np.matrix(q).T 
                        isThereInterCollision[k,i] = not any(inter_collision_inequality)
                        if isThereInterCollision[k,i]:
                            print('Inter time step collision detected at time step: ', k)
                            break
                 




                                 
        #print('n_collisions: ', number_of_collisions)
        number_of_collisions = np.sum(isThereCollision) + np.sum(isThereInterCollision)
        return number_of_collisions
                
    def check_connectivity_loss(self,n):

        # This values are mannually transfered from matlab, try to automate somehow
        p1 = 0.1000
        p2 = 0.2413
        q1 = 0.9653

        epsilon = 0.01
        P = np.matrix([[-p1,-p2],
                       [p1,-p2],
                       [p2,-p1],
                       [p2,p1],
                       [p1,p2],
                       [-p1,p2],
                       [-p2,p1],
                       [-p2,-p1]]) 
        
        q = np.array([  q1+epsilon,
                        q1+epsilon,
                        q1+epsilon,
                        q1+epsilon,
                        q1+epsilon,
                        q1+epsilon,
                        q1+epsilon,
                        q1+epsilon])

        # External agent positions
        ext_agt_pos = np.stack(self.ext_agt.cont_traj,axis=0)
        ext_agt_pos[:, [0, 1]] = ext_agt_pos[:, [1, 0]] # ext_agt_pos is inverted in x,y - inverts back
        traj_len = len(ext_agt_pos)
        n_robots = len(self.mrs.robot)

        isConnected = np.zeros([traj_len,n_robots])
        wasConnLost = False
        for i in range(n_robots):
            robots_pos = np.stack(self.mrs.robot[i]['pos'],axis=0)

            for k in range(traj_len):
                # Verifies if agent 'i' is within range of the human
                rel_pos = ext_agt_pos[k,:] - robots_pos[k,:] 
                connectivity_inequality = P*np.matrix(rel_pos).T <= np.matrix(q).T 
                isConnected[k,i] = all(connectivity_inequality)

        for k in range(traj_len):
            if np.sum(isConnected[k,:]) < 1.0:
                wasConnLost = True
                print('Connectivity lost at time step: ', k, ' of experiment', n)
                break

        return wasConnLost
            
    def check_target_collection(self):

        n_agents = len(self.mrs.robot) + 1 # +Human
        WTV = np.zeros([self.mp.n_targets-1,1])
        for i in range(n_agents):

            if i < n_agents-1: # robots
                agent_pos = np.stack(self.mrs.robot[i]['pos'],axis=0)
            else: # human
                agent_pos = np.stack(self.ext_agt.cont_traj,axis=0)
                agent_pos[:, [0, 1]] = agent_pos[:, [1, 0]] # ext_agt_pos is inverted in x,y - inverts back


            for k in range(len(agent_pos)):
                for j in range(self.mp.n_targets-1):
                    if not WTV[j][0]: # if not already visited
                        WTV[j][0] = all(self.mp.P*np.matrix(agent_pos[k,:]).T <= np.matrix(self.mp.q[j]).T) 

            
        if np.sum(WTV) < 4.0:
            stop=1                
        return np.sum(WTV)
            
    def check_redundant_collection(self):
        
        # Check number of collects per target
        n_redund_collects_per_tar = np.zeros([self.mp.n_targets,1])
        for j in range(self.mp.n_targets-1):
            if np.sum(self.mp.WTV_by_agent[:,j]) > 0.0:
                n_redund_collects_per_tar[j] = np.sum(self.mp.WTV_by_agent[:,j]) - 1.0

        total_n_redund_collects = sum(n_redund_collects_per_tar)
        return total_n_redund_collects

        

    