
from external_agent import *
from analysis import *
from motion_planner import *
from multirobot_system import *
import dill
import parameters
import scipy.io as sp
from scipy.stats import bootstrap

# although its called environment-1 it is used for environment 1 and 2. 
# The difference in envs 1 and 2 are just the target and agent placements, which are done via parameter.py
# To change obstacles and terminal region, see 'Environment2-v0'
env_id = 'Environment1-v0' 
#env_id = 'SmallMuseumCollect-v0' # make sure its registered at ~/.local/lib/python3.8/site-packages/gym/envs

ext_agt_model_id = './pytorch_models/less_mov_penality_6hours.pt'

#prediction_model_id = './pytorch_models/sleek-dust-253.pt' # Prediction model from AIRL
prediction_model_id = './pytorch_models/generator_less_mov_penality_6hours.pt' 

mpc_model_id = "./gurobi_models/MPCModel_environment2_new.mps" # MPC-MIP model from MATLAB
#mpc_model_id = "./gurobi_models/MPCModel_twoRobots_updated_cornerCut_firstTimeStep.mps"

env_params, mrs_params, matlab_params = parameters.get_parameters('environment2')

# set env_params to none if you want to randomize environment!!!
#env_params = None
ext_agt = ExternalAgent(env_id,ext_agt_model_id,env_params)
motion_planner = MotionPlanner(env_id,prediction_model_id,mpc_model_id,env_params,matlab_params,ext_agt)
mrs = MultiRobotSystem(mrs_params)

# Analysis --------------------------------------------


# UNCOOPERATIVE ROBOT-HUMAN SCENARIO
analysis = Analysis(ext_agt,mrs,motion_planner)

# Load MATLAB stuff (uncooperative robots trajectories)
#matlab_data = sp.loadmat('./uncoop_robots_trajs/uncoop_trajs.mat', squeeze_me=True)
matlab_data = sp.loadmat('./uncoop_robots_trajs/uncoop_trajs_reward10.mat', squeeze_me=True)

# Load python stuff (human trajectory)
#data_file = './mpc_irl_data.pckl' # closed-loop env1
#data_file = './mpc_irl_data_env2.pckl' # closed-loop env2
#isClosedLoop = True # required for plots
data_file = 'mip_data_env2.pckl' # open-loop
isClosedLoop = False # required for plots
#plotPredictions = False


data = analysis.import_data_for_analysis(data_file)
number_of_experiments = len(data)

eval_metrics = {
    'n_col': np.zeros([number_of_experiments,1]),
    'n_conn_loss': np.zeros([number_of_experiments,1]),
    'n_tar': np.zeros([number_of_experiments,1]),
    'redundant_collections': np.zeros([number_of_experiments,1])
}

save_snapshots = True
plot_snapshots = False
compute_stats = False
plotPredictions = False
#for n in range(number_of_experiments):
for n in range(501,1000):

    print("Experiment: ", n)
    analysis.incorporate_exp_data(data[n]) # assings data from human from experiments
    #analysis.incorporate_matlab_data(matlab_data) # run this for human-robot noncoop
    analysis.remove_robots_data() # run this for human alone

    if compute_stats:
        eval_metrics['n_col'][n][0] = analysis.check_collisions()
        eval_metrics['n_conn_loss'][n][0] = analysis.check_connectivity_loss(n)
        eval_metrics['n_tar'][n][0] = analysis.check_target_collection()
        #if eval_metrics ['n_tar'][n][0] < 4.0:
            #print("not all targets visited in exp: ", n)
        eval_metrics['redundant_collections'][n][0] = analysis.check_redundant_collection()

    if save_snapshots or plot_snapshots:
        analysis.plot_snapshots(save_snapshots,plot_snapshots,n,isClosedLoop,plotPredictions)

if compute_stats:
    print('Average number of collisions: ', np.mean(eval_metrics['n_col']))
    bs = bootstrap( (eval_metrics['n_col'],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
    lower_CI = bs.confidence_interval[0]
    upper_CI = bs.confidence_interval[1]
    print('CI collisions: (',lower_CI, ',',upper_CI,')' ) 

    print('Average number of disconnections: ', np.mean(eval_metrics['n_conn_loss']))
    bs = bootstrap( (eval_metrics['n_conn_loss'],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
    lower_CI = bs.confidence_interval[0]
    upper_CI = bs.confidence_interval[1]
    print('CI disconnections: (',lower_CI, ',',upper_CI,')' ) 

    print('Average number of targets: ', np.mean(eval_metrics['n_tar']))   
    bs = bootstrap( (eval_metrics['n_tar'],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
    lower_CI = bs.confidence_interval[0]
    upper_CI = bs.confidence_interval[1]
    print('CI number of targets: (',lower_CI, ',',upper_CI,')' ) 

    print('Average number of redundant_collections: ', np.mean(eval_metrics['redundant_collections']))   
    bs = bootstrap( (eval_metrics['redundant_collections'],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
    lower_CI = bs.confidence_interval[0]
    upper_CI = bs.confidence_interval[1]
    print('CI redundant_collections: (',lower_CI, ',',upper_CI,')' ) 



