
import numpy as np

class MultiRobotSystem:

    def __init__(self,params):
        
        self.n_robots = params['number_of_robots']
        self.Ts = params['sampling_period']
        self.initial_state = params['initial_state']
        self.robot_length = params['robot_length']
        self.robot_width = params['robot_width']


        # List of dictionaries with robot logged data
        self.robot = []
        for i in range(self.n_robots):
            self.robot.append( {
                'pos': [],
                'vel': [],
                'acc': [],
                'pred_pos': [],
                'pred_vel': []
            })

        # dynamics
        if params['dynamics'] == "double_integrator":

            Ad = np.matrix([
                [1.0, self.Ts, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],    
                [0.0, 0.0, 1.0, self.Ts],   
                [0.0, 0.0, 0.0, 1.0]   
            ]) 
            Bd = np.matrix([
                [self.Ts*self.Ts/2, 0.0],
                [self.Ts, 0.0],
                [0.0, self.Ts*self.Ts/2],
                [0.0, self.Ts]
            ])
            Cd = np.matrix([
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0]
            ])

            self.dyn = {
                'Ad': Ad,
                'Bd': Bd,
                'Cd': Cd
            }


    def forward_dynamics(self,x_fb,u_k):

        x_fb_i_next = np.matrix(np.zeros([len(self.dyn['Ad']),self.n_robots]))
        for i in range(self.n_robots):
            x_fb_i = x_fb[:,i]
            u_k_i = np.matrix(u_k[:,i]).T
            x_fb_i_next[:,i] = self.dyn['Ad']*x_fb_i + self.dyn['Bd']*u_k_i

        
        return x_fb_i_next


    def save_data(self,x_k,u_k,mrs_pred_traj):

      
        for i in range(self.n_robots):
            self.robot[i]['pos'].append([x_k[0,i],x_k[2,i]])
            self.robot[i]['vel'].append([x_k[1,i],x_k[3,i]])
            self.robot[i]['acc'].append([u_k[0,i],u_k[1,i]])

            self.robot[i]['pred_pos'].append([mrs_pred_traj[0,:,i], mrs_pred_traj[2,:,i] ])
            self.robot[i]['pred_vel'].append([mrs_pred_traj[1,:,i], mrs_pred_traj[3,:,i] ])

   