from tqdm import tqdm
from ppo import *
import torch
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from airl import *
import gym
import gym_examples
import pickle
import matplotlib.pyplot as plt
from scipy.stats import bootstrap

# Use GPU if available
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

if __name__ == '__main__':
    
    # Create Environment
    env_id = 'SmallMuseumCollect-v0'
    env = gym.make("gym_examples/"+env_id)
    states = env.reset()
    states_tensor = torch.tensor(states).float().to(device)
    gamma = 0.999

    # Fetch Shapes
    n_actions = env.action_space.n
    obs_shape = env.observation_space.shape
    state_shape = obs_shape[1:]
    in_channels = obs_shape[0]


    # Select n demos (need to set timer separately)
    n_demos = 2000

    max_steps = 50
    n_ep_per_scenario = 40.0 # since the policies are stochastic, we must repeat the episodes and take avg

    # PPO
    ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
    ppo.load_state_dict(torch.load('./saved_policies/expert_policy.pt'))


    ##################################################################################
    # ATTENTION: FOR THIS CODE TO WORK YOU MUST GO TO the 'reset' function in 'small_musem_collect_v0.py' 
    # and uncomment 'p.random.seed(self.reset_counter)' in line 225 !!!!!!!!!!!!!!!!!!!!
    ##################################################################################
    metrics_per_env = []
    ci_per_env = []
    switch_performed = False
    

    isThereCollision = False
    while len(metrics_per_env ) <= n_demos: 


        if len(metrics_per_env)%100 == 0:
            print(len(metrics_per_env))

        # resets counters
        metrics = {'avg_n_tar_A_visit': [], 'avg_n_tar_B_visit': [], 'avg_n_coll': [], 'avg_ep_length': [], 'avg_return': []}
        ci = {'avg_return': []}
        
        # Switches policy uppon reaching half demos
        if len(metrics_per_env) >= n_demos/2.0 and not switch_performed:
            ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
            #ppo.load_state_dict(torch.load('./saved_generators/sleek-dust-253.pt')) # previous one
            ppo.load_state_dict(torch.load('./saved_generators/recovered_policy.pt'))
            
            env.refresh_reset_counter()
            switch_performed = True
            states = env.reset()
            env.update_state(states)
        

        # Only refreshes now
        return_per_ep = []
        n_tar_A_visit = 0
        n_tar_B_visit = 0
        n_coll = 0
        total_time_steps = 0

        # Episode loop
        for _ in range(int(n_ep_per_scenario)):

            return_ = 0

            # Dynamics
            for k in range(max_steps):
                actions, log_probs = ppo.act(states_tensor)
                next_states, rewards, done, _ = env.step(actions[0]) # done is related to 'k' (states) not 'k+1' (next_states)
                next_states = np.stack(next_states, axis=0)

                # count metrics: 
                if env.flag_tar_A: # number of target A and B targets visited
                    n_tar_A_visit = n_tar_A_visit + 1
                    
                if env.flag_tar_B: # number of collisions
                    n_tar_B_visit = n_tar_B_visit + 1
            
                if env.flag_coll: # episode length
                    n_coll = n_coll + 1
                    

                return_ = return_ + np.sum(rewards)
            

                # termination
                if done: 
                    total_time_steps = total_time_steps + k
                    states = env.state.copy()
                    states_tensor = torch.tensor(states).float().to(device)
                    return_per_ep.append(return_)
                    break

                states = next_states.copy()
                states_tensor = torch.tensor(states).float().to(device)
        

        # save metrics
        metrics['avg_n_tar_A_visit'] = n_tar_A_visit/n_ep_per_scenario
        metrics['avg_n_tar_B_visit'] = n_tar_B_visit/n_ep_per_scenario
        metrics['avg_n_coll'] = n_coll/n_ep_per_scenario
        metrics['avg_ep_length'] = total_time_steps/n_ep_per_scenario
        metrics['avg_return'] = np.sum(return_per_ep)/n_ep_per_scenario
        if metrics['avg_return'] < -5.0:
           print('Extreme case! Seed: ',env.reset_counter)
                   
        ci['avg_return'] = bootstrap( (return_per_ep,) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')

        metrics_per_env.append(metrics)
        ci_per_env.append(ci)

        # after averages for this environment are taken, update seed to generate new one
        env.update_seed()


    # Recover confidence intervals from bootstrap class (picke does not save bootstrap classes)
    # Expert mean return and CI
    ci_lower_expert = []
    ci_upper_expert = []
    for i in range(int((len(ci_per_env)-1)/2)):
        ci_lower_expert.append(ci_per_env[i]['avg_return'].confidence_interval[0])
        ci_upper_expert.append(ci_per_env[i]['avg_return'].confidence_interval[1])

    # AIRL mean return and CI
    ci_lower_AIRL = []
    ci_upper_AIRL = []
    for i in range(int((len(ci_per_env)-1)/2),len(ci_per_env)-1):
        ci_lower_AIRL.append(ci_per_env[i]['avg_return'].confidence_interval[0])
        ci_upper_AIRL.append(ci_per_env[i]['avg_return'].confidence_interval[1])

    ci_for_picke = {
        'ci_lower_expert': ci_lower_expert,
        'ci_upper_expert': ci_upper_expert,
        'ci_lower_AIRL': ci_lower_AIRL,
        'ci_upper_AIRL': ci_upper_AIRL
    }

    ### Plots ------------------------------------------------------------------------------------------------- 
    #pickle.dump(metrics_per_env, open('./monte_carlo_policy_eval/metrics_data_for_plot.pk', 'wb'))
  
    half_n_demos = n_demos/2.0

    pickle.dump([metrics_per_env,ci_for_picke], open('./monte_carlo_policy_eval/metrics_data_for_plot_' + str(half_n_demos) + 'demos_new.pk', 'wb'))

    plt.figure(1)
    aux = list(map(lambda x: x["avg_ep_length"], metrics_per_env))
    plt.plot(aux[0:int(n_demos/2)],"-b",label='Expert')
    plt.plot(aux[int(n_demos/2):-1],"--r",label='AIRL')
    plt.legend(loc="upper right")
    plt.grid()
    plt.xticks(np.arange(0, n_demos+1, step=int(n_demos/5)))
    plt.xlabel('Environment')
    plt.ylabel('Average Trajectory Length')

    plt.figure(2)
    aux = list(map(lambda x: x["avg_n_tar_A_visit"], metrics_per_env))
    plt.plot(aux[0:int(n_demos/2)],"-b",label='Expert')
    plt.plot(aux[int(n_demos/2):-1],"--r",label='AIRL')
    plt.legend(loc="upper right")
    plt.grid()
    plt.xticks(np.arange(0, n_demos+1, step=int(n_demos/5)))
    plt.xlabel('Environment')
    plt.ylabel('Average Target A Collection')
    plt.ylim(-1,4)
  

    plt.figure(3)
    aux = list(map(lambda x: x["avg_n_tar_B_visit"], metrics_per_env))
    plt.plot(aux[0:int(n_demos/2)],"-b",label='Expert')
    plt.plot(aux[int(n_demos/2):-1],"--r",label='AIRL')
    plt.legend(loc="upper right")
    plt.grid()
    plt.xticks(np.arange(0, n_demos+1, step=int(n_demos/5)))
    plt.xlabel('Environment')
    plt.ylabel('Average Target B Collection')
    plt.ylim(-1,4)


    plt.figure(4)
    aux = list(map(lambda x: x["avg_n_coll"], metrics_per_env))
    plt.plot(aux[0:int(n_demos/2)],"-b",label='Expert')
    plt.plot(aux[int(n_demos/2):-1],"--r",label='AIRL')
    plt.legend(loc="upper right")
    plt.grid()
    plt.xticks(np.arange(0, n_demos+1, step=int(n_demos/5)))
    plt.xlabel('Environment')
    plt.ylabel('Average Collisions')
    plt.ylim(-1,4)

    plt.figure(5)
    aux = list(map(lambda x: x["avg_return"], metrics_per_env))
    x = np.linspace(0,(n_demos/2)-1,int(n_demos/2))
    y = aux[0:int(n_demos/2)]



    plt.plot(x,y,"-b",label='Expert')
    plt.fill_between(x,ci_lower_expert,ci_upper_expert,color='b',alpha=0.1)



    
    y = aux[int(n_demos/2):-1]
    plt.plot(x,y,"--r",label='AIRL')
    plt.fill_between(x,ci_lower_AIRL,ci_upper_AIRL,color='r',alpha=0.1)

    plt.legend(loc="upper right")
    plt.grid()
    plt.xticks(np.arange(0, n_demos+1, step=int(n_demos/5)))
    plt.xlabel('Environment')
    plt.ylabel('Average Return')
    #plt.ylim(-1,4)

    plt.show()



   
    
    


   

