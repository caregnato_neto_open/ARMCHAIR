from tqdm import tqdm
from ppo import PPO, TrajectoryDataset, update_policy
from stable_baselines3.common.vec_env import SubprocVecEnv
#from gym_wrapper import *
from airl import *
import torch
import numpy as np
import pickle
import wandb
import gym_examples
import gym
from stable_baselines3.common.utils import set_random_seed

# Device Check
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

if __name__ == '__main__':

    # REQUIRED FOR MULTI-THREADING
    def make_env(env_id, rank, seed=0):
        """
        Utility function for multiprocessed env.
    
        :param env_id: (str) the environment ID
        :param seed: (int) the inital seed for RNG
        :param rank: (int) index of the subprocess  
        """
        def _init():
            env = gym.make(env_id)
            # Important: use a different seed for each environment
            env.seed(seed + rank)
            return env
        set_random_seed(seed)
        return _init


    # Load demonstrations
    expert_trajectories = pickle.load(open('./demos/expert_demo.pk', 'rb')) 
    
    # Init WandB & Parameters
    wandb.init(project='AIRL', config={
        'env_id': 'small_multitarget_v0',
        'env_steps': 3500000,
        'batchsize_discriminator': 256, 
        'batchsize_ppo': 8, 
        'n_workers': 8, 
        'entropy_reg': 0.03, #
        'gamma': 0.999,
        'epsilon': 0.1,
        'ppo_epochs': 5
    })
    config = wandb.config

    # Create Environment
    vec_env = SubprocVecEnv([make_env("gym_examples/SmallMuseumCollect-v0", i) for i in range(config.n_workers)]) 

    states = vec_env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = vec_env.action_space.n
    obs_shape = vec_env.observation_space.shape
    state_shape = obs_shape[1:]
    in_channels = obs_shape[0]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, n_actions=n_actions, in_channels=in_channels).to(device)
    discriminator = DiscriminatorMLP(state_shape=state_shape, in_channels=in_channels).to(device)

    optimizer = torch.optim.Adam(ppo.parameters(), lr=4e-4) # 4e-4
    optimizer_discriminator = torch.optim.Adam(discriminator.parameters(), lr=4e-4)
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)

    # Logging
    objective_logs = []

    for t in tqdm(range((int(config.env_steps/config.n_workers)))):
        
        actions, log_probs = ppo.act(states_tensor)
        next_states, rewards, dones, _  = vec_env.step(actions)

        # Data treatment
        next_states = np.stack(next_states, axis=0) # correct format

        # Log Objectives
        objective_logs.append(rewards)

        # Calculate (vectorized) AIRL reward
        airl_state = torch.tensor(states).to(device).float()
        airl_next_state = torch.tensor(next_states).to(device).float()
        airl_action_prob = torch.exp(torch.tensor(log_probs)).to(device).float()

        for i in range(config.n_workers):
            #"""
            if not dones[i]:
                airl_rewards = discriminator.predict_reward(airl_state, airl_next_state, config.gamma, airl_action_prob) # \hat{r}
            else: # overwrittes reset from env to register zeroed reward matrix
                state_aux = states.copy()
                state_aux[0][1] = np.zeros((7,7))
                state_aux[0][2] = np.zeros((7,7))
                test_airl_state = torch.tensor(state_aux).to(device).float()
                airl_rewards = discriminator.predict_reward(airl_state, test_airl_state, config.gamma, airl_action_prob)
            #"""

        airl_rewards = list(airl_rewards.detach().cpu().numpy() * [0 if i else 1 for i in dones])

        # Save Trajectory
        train_ready = dataset.write_tuple(states, actions, airl_rewards, dones, log_probs, rewards) # GENERATOR trajectories

        if train_ready:
            
            # Log Objectives
            #objective_logs = np.array(objective_logs).sum(axis=0)

            #Hack: the movement penalization log keeps registering penalizations after the done signal (only a logging problem)
            objective_logs = dataset.log_objectives()
            for j in range(config.batchsize_ppo):
                traj_length = len(dataset.trajectories[j]['rewards'])
                objective_logs[j, 0] = rewards[0,0]*traj_length
            # END OF hack SOLUTION

            #for i in range(objective_logs.shape[1]):
                #wandb.log({'Obj_' + str(i): objective_logs[:, i].mean()})
            wandb.log({'Movement penalties' : objective_logs[:, 0].mean()})
            wandb.log({'Collision penalties' : objective_logs[:, 3].mean()})
            #wandb.log({'Target A rewards' : objective_logs[:, 2].mean()})
            #wandb.log({'Target B rewards' : objective_logs[:, 3].mean()})
            wandb.log({'Target A rewards' : objective_logs[:, 1].mean()})
            wandb.log({'Target B rewards' : objective_logs[:, 2].mean()})
            objective_logs = []
            
            # Update Models
            update_policy(ppo, dataset, optimizer, config.gamma, config.epsilon, config.ppo_epochs,
                          entropy_reg=config.entropy_reg)
            d_loss, fake_acc, real_acc = update_discriminator(discriminator=discriminator,
                                                              optimizer=optimizer_discriminator,
                                                              gamma=config.gamma,
                                                              expert_trajectories=expert_trajectories,
                                                              policy_trajectories=dataset.trajectories.copy(), ppo=ppo,
                                                              batch_size=config.batchsize_discriminator)

            # Log Loss Statsitics
            wandb.log({'Discriminator Loss': d_loss,
                       'Fake Accuracy': fake_acc,
                       'Real Accuracy': real_acc})
            for ret in dataset.log_returns():
                wandb.log({'Returns': ret})
            dataset.reset_trajectories()

       # This is a continuation of the sampling part
        # Prepare state input for next time step
        if not train_ready:
            states = next_states.copy()
            states_tensor = torch.tensor(states).float().to(device)

    vec_env.close()
    #torch.save(discriminator.state_dict(), './saved_discriminators/teste_ENTRONONZERO.pt')
    #torch.save(ppo.state_dict(),'./saved_generators/teste_ENTRONONZERO.pt')


                                                       